function ejercicio05(numeros) 
{
    var sumas=[];
    var contador=0;
    
    for(var i=0;i<numeros.length;i++)
    {
        for(var j=0;j<numeros[i].length;j++)
        {
            contador+=numeros[i][j];
        }
        sumas.push(contador);
        contador=0;
    }
    
    return sumas;
}
